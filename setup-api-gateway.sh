#!/bin/bash

# Variables (replace these with your values)
AWS_REGION="ap-south-1"
LAMBDA_FUNCTION_ARN="arn:aws:lambda:ap-south-1:851725599785:function:selenium-b"
API_NAME="SeleniumScraperAPI"
API_DESCRIPTION="API for Selenium Scraper Lambda"

# Create the API
REST_API_ID=$(aws apigateway create-rest-api --name "$API_NAME" --description "$API_DESCRIPTION" --region "$AWS_REGION" --query 'id' --output text)
echo "Created API with ID: $REST_API_ID"

# Get the Root Resource ID
ROOT_RESOURCE_ID=$(aws apigateway get-resources --rest-api-id "$REST_API_ID" --region "$AWS_REGION" --query 'items[?path==`/`].id' --output text)
echo "Root Resource ID: $ROOT_RESOURCE_ID"

# Function to create methods
create_method_and_integration() {
    local RESOURCE_ID=$1
    local HTTP_METHOD=$2
    aws apigateway put-method --rest-api-id "$REST_API_ID" --resource-id "$RESOURCE_ID" --http-method "$HTTP_METHOD" --authorization-type "NONE" --region "$AWS_REGION"
    echo "Created $HTTP_METHOD method for Resource ID: $RESOURCE_ID"
    aws apigateway put-integration --rest-api-id "$REST_API_ID" --resource-id "$RESOURCE_ID" --http-method "$HTTP_METHOD" --type AWS_PROXY --integration-http-method POST --uri "arn:aws:apigateway:$AWS_REGION:lambda:path/2015-03-31/functions/$LAMBDA_FUNCTION_ARN/invocations" --region "$AWS_REGION"
    echo "Integrated $HTTP_METHOD method for Resource ID: $RESOURCE_ID with Lambda"
}

# Create the root method and integration
create_method_and_integration "$ROOT_RESOURCE_ID" "GET"

# Create Resources under /v1
V1_RESOURCE_ID=$(aws apigateway create-resource --rest-api-id "$REST_API_ID" --parent-id "$ROOT_RESOURCE_ID" --path-part v1 --region "$AWS_REGION" --query 'id' --output text)
echo "Created /v1 Resource with ID: $V1_RESOURCE_ID"

# Function to create sub-resources and methods
create_sub_resource_and_method() {
    local SUB_RESOURCE_NAME=$1
    local HTTP_METHOD=$2
    local SUB_RESOURCE_ID=$(aws apigateway create-resource --rest-api-id "$REST_API_ID" --parent-id "$V1_RESOURCE_ID" --path-part "$SUB_RESOURCE_NAME" --region "$AWS_REGION" --query 'id' --output text)
    echo "Created /v1/$SUB_RESOURCE_NAME Resource with ID: $SUB_RESOURCE_ID"
    create_method_and_integration "$SUB_RESOURCE_ID" "$HTTP_METHOD"
}

# Create sub-resources and methods
create_sub_resource_and_method "district" "GET"
create_sub_resource_and_method "tahasil" "GET"
create_sub_resource_and_method "villages" "GET"
create_sub_resource_and_method "tenant" "POST"
create_sub_resource_and_method "tenant-file" "POST"

# Deploy the API
DEPLOYMENT_ID=$(aws apigateway create-deployment --rest-api-id "$REST_API_ID" --stage-name prod --region "$AWS_REGION" --query 'id' --output text)
echo "Deployed API with Deployment ID: $DEPLOYMENT_ID"

# Add Permissions for API Gateway to Invoke Lambda
STATEMENT_ID="apigateway-access-$(date +%s)"
aws lambda add-permission --function-name "$(basename "$LAMBDA_FUNCTION_ARN")" --statement-id "$STATEMENT_ID" --action lambda:InvokeFunction --principal apigateway.amazonaws.com --source-arn "arn:aws:execute-api:$AWS_REGION:$(aws sts get-caller-identity --query 'Account' --output text):$REST_API_ID/*/*/*" --region "$AWS_REGION"
echo "Added permissions for API Gateway to invoke Lambda with statement ID: $STATEMENT_ID"

echo "API Gateway setup complete. Invoke URL: https://${REST_API_ID}.execute-api.${AWS_REGION}.amazonaws.com/prod"
