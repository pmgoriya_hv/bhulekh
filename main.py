from serverless_wsgi import handle_request
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from flask import Flask, request, jsonify, make_response
from bs4 import BeautifulSoup
from fuzzywuzzy import fuzz
from flask_cors import CORS
import os
import logging
import pdfkit
from time import sleep
from tempfile import mkdtemp
import json

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

app = Flask(__name__)
CORS(app, supports_credentials=True)

@app.route("/", methods=["GET"])
def index():
    return jsonify({"hello": "hello, world"})

scrape_url = "https://bhulekh.ori.nic.in/Default.aspx"

def optionsInitialize():
    chrome_options = ChromeOptions()
    chrome_options.add_argument("--headless=new")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-dev-tools")
    chrome_options.add_argument("--no-zygote")
    chrome_options.add_argument("--single-process")
    chrome_options.add_argument(f"--user-data-dir={mkdtemp()}")
    chrome_options.add_argument(f"--data-path={mkdtemp()}")
    chrome_options.add_argument(f"--disk-cache-dir={mkdtemp()}")
    chrome_options.add_argument("--remote-debugging-pipe")
    chrome_options.add_argument("--verbose")
    chrome_options.add_argument("--log-path=/tmp")
    chrome_options.binary_location = "/opt/chrome/chrome-linux64/chrome"
    
    service = Service(executable_path="/opt/chrome-driver/chromedriver-linux64/chromedriver")
    driver = webdriver.Chrome(service=service, options=chrome_options)
    return driver

def elementFind(driver: webdriver.Chrome, value: str, dropdownValue: str, id_to_find: str, is_option: bool = True):
    dropdown = driver.find_element(By.NAME, dropdownValue)
    driver.execute_script(
        f"arguments[0].value='{value}';arguments[0].dispatchEvent(new Event('change'));", dropdown)
    WebDriverWait(driver, 4).until(
        EC.presence_of_element_located(
            (By.XPATH, f'//*[@id="{id_to_find}"]{"/option[2]" if is_option else ""}'))
    )
    return driver.find_element(By.ID, id_to_find)

def optionListGenerator(element):
    options_tags_element = element.find_elements(By.TAG_NAME, "option")
    return [
        {"value": element.get_attribute("value").strip(), "label": f"{index+1}. {element.text.strip()}"}
        for index, element in enumerate(options_tags_element)
    ]

@app.route("/v1/district", methods=["GET"])
def district_scrape():
    logger.info("Initializing WebDriver for district scraping")
    driver = optionsInitialize()
    try:
        logger.info(f"Accessing URL: {scrape_url}")
        driver.get(scrape_url)
        logger.info("Successfully accessed URL")
        district = driver.find_element(By.NAME, "ctl00$ContentPlaceHolder1$ddlDistrict")
        options = optionListGenerator(district)
        return jsonify({"districts": options})
    except Exception as e:
        logger.error(f"Error during district scraping: {e}", exc_info=True)
        return jsonify({"error": str(e)})
    finally:
        driver.quit()

@app.route("/v1/tahasil", methods=["GET"])
def tahasil_scrape():
    value = request.args["district"]
    logger.info(f"Received district: {value}")
    driver = optionsInitialize()
    try:
        driver.get(scrape_url)
        logger.info(f"Accessing URL: {scrape_url}")
        tahasil_element = elementFind(driver, value, "ctl00$ContentPlaceHolder1$ddlDistrict", "ctl00_ContentPlaceHolder1_ddlTahsil")
        tahasil_options = optionListGenerator(tahasil_element)
        return jsonify({"tahasil": tahasil_options})
    except Exception as e:
        logger.error(f"Error during tahasil scraping: {e}", exc_info=True)
        return jsonify({"error": str(e)})
    finally:
        driver.quit()

@app.route("/v1/villages", methods=["GET"])
def village_scrape():
    district = request.args["district"]
    tahasil = request.args["tahasil"]
    logger.info(f"Received district: {district}, tahasil: {tahasil}")
    driver = optionsInitialize()
    try:
        driver.get(scrape_url)
        logger.info(f"Accessing URL: {scrape_url}")
        elementFind(driver, district, "ctl00$ContentPlaceHolder1$ddlDistrict", "ctl00_ContentPlaceHolder1_ddlTahsil")
        village_element = elementFind(driver, tahasil, "ctl00$ContentPlaceHolder1$ddlTahsil", "ctl00$ContentPlaceHolder1$ddlVillage")
        village_options = optionListGenerator(village_element)
        return jsonify({"villages": village_options})
    except Exception as e:
        logger.error(f"Error during village scraping: {e}", exc_info=True)
        return jsonify({"error": str(e)})
    finally:
        driver.quit()

@app.route("/v1/tenant", methods=["POST"])
def tenant_scrape():
    data = request.json
    district = data["district"]
    tahasil = data["tahasil"]
    village = data["village"]
    tenant_name = data["tenant_name"]
    driver = optionsInitialize()
    oriya_converted = tenant_name
    farmerName = oriya_converted
    logger.info(f"Received data: district={district}, tahasil={tahasil}, village={village}, tenant_name={tenant_name}")
    try:
        driver.get(scrape_url)
        elementFind(driver, district, "ctl00$ContentPlaceHolder1$ddlDistrict", "ctl00_ContentPlaceHolder1_ddlTahsil")
        elementFind(driver, tahasil, "ctl00$ContentPlaceHolder1$ddlTahsil", "ctl00$ContentPlaceHolder1$ddlVillage")
        dropdown = driver.find_element(By.NAME, "ctl00$ContentPlaceHolder1$ddlVillage")
        driver.execute_script(
            f"arguments[0].value='{village}';arguments[0].dispatchEvent(new Event('change'));", dropdown)
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_lblColumnName"]'))
        )
        tenant_element = driver.find_element(By.ID, "ctl00_ContentPlaceHolder1_rbtnRORSearchtype_2")
        tenant_element.click()
        WebDriverWait(driver, 10).until(
            EC.text_to_be_present_in_element((By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_lblColumnName"]'), "Select Tenant"))
        tenant_dropdown_element = Select(driver.find_element(By.ID, "ctl00_ContentPlaceHolder1_ddlBindData"))
        list_of_farmers = [
            {"search": element.text.strip().split("(")[0], "value": element.text.strip()}
            for element in tenant_dropdown_element.options
        ]
        bestOption = searchingFarmerName(farmerName, list_of_farmers)
        return jsonify({"farmers_found": bestOption, "translated_name": farmerName})
    except Exception as e:
        logger.error(f"Error during tenant scraping: {e}", exc_info=True)
        return jsonify({"error": str(e)})
    finally:
        driver.quit()

@app.route("/v1/tenant-file", methods=["POST"])
def getTenantFile():
    driver = optionsInitialize()
    data = request.json
    district = data["district"]
    tahasil = data["tahasil"]
    village = data["village"]
    tenant_name = data["tenant_name"]
    accuracy = data["accuracy"]
    english_name = data["english_name"]
    logger.info(f"Received data: district={district}, tahasil={tahasil}, village={village}, tenant_name={tenant_name}, accuracy={accuracy}, english_name={english_name}")
    try:
        driver.get(scrape_url)
        elementFind(driver, district, "ctl00$ContentPlaceHolder1$ddlDistrict", "ctl00_ContentPlaceHolder1_ddlTahsil")
        elementFind(driver, tahasil, "ctl00$ContentPlaceHolder1$ddlTahsil", "ctl00$ContentPlaceHolder1$ddlVillage")
        dropdown = driver.find_element(By.NAME, "ctl00$ContentPlaceHolder1$ddlVillage")
        driver.execute_script(
            f"arguments[0].value='{village}';arguments[0].dispatchEvent(new Event('change'));", dropdown)
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_lblColumnName"]'))
        )
        tenant_element = driver.find_element(By.ID, "ctl00_ContentPlaceHolder1_rbtnRORSearchtype_2")
        tenant_element.click()
        WebDriverWait(driver, 10).until(
            EC.text_to_be_present_in_element((By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_lblColumnName"]'), "Select Tenant"))
        tenant_dropdown_element = Select(driver.find_element(By.ID, "ctl00_ContentPlaceHolder1_ddlBindData"))
        tenant_dropdown_element.select_by_visible_text(tenant_name)
        driver.find_element(By.NAME, "ctl00$ContentPlaceHolder1$btnRORFront").click()
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="rorfrontheading"]'))
        )
        driver.find_element(By.ID, "btnPrint").click()
        driver.switch_to.window(driver.window_handles[1])
        soup = BeautifulSoup(driver.page_source, "html.parser")
        farmer_name = tenant_name.split("(")[0]
        span_farmer_tag = soup.find("span", attrs={"id": "gvfront_ctl02_lblName"})
        span_text = span_farmer_tag.text
        splitted_converted_name = farmer_name.split(" ")
        splitted_converted_name.reverse()
        farmer_name = " ".join(splitted_converted_name)
        updated_text = span_text.replace(
            farmer_name, f'<span style="text-decoration: underline;">{farmer_name} ({english_name} - {accuracy}%)</span>')
        span_farmer_tag.clear()
        span_farmer_tag.append(BeautifulSoup(updated_text, "html.parser"))
        pdf = pdfkit.from_string(soup.prettify(), False, options={"encoding": "UTF-8"})
        response = make_response(pdf)
        response.headers["Content-Type"] = "application/pdf"
        response.headers["content-disposition"] = (
            f"attachment; filename={english_name.encode('utf-8')}-land-details-.pdf"
        )
        return response
    except Exception as e:
        logger.error(f"Error during tenant file generation: {e}", exc_info=True)
        return jsonify({"error": "Error"})
    finally:
        driver.quit()

def searchingFarmerName(farmerName: str, list_of_farmers: list):
    response = []
    accuracy = 0
    splitted_name = farmerName.split(" ")
    splitted_name.reverse()
    reversed_name = " ".join(splitted_name)
    for farmer in list_of_farmers:
        accuracy = fuzz.ratio(reversed_name, farmer["search"])
        response.append({"farmers_name": farmer["search"], "percentage": accuracy, "value": farmer["value"]})
    return sorted(response, key=lambda r: r["percentage"], reverse=True)

def lambda_handler(event, context):
    return handle_request(app, event, context)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
